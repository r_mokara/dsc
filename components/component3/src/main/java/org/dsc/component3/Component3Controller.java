package org.dsc.component3;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/component1")
public class Component3Controller {

	@GetMapping("/")
	public String getData() {
		return "Component3Controller updated on 27th May 2019 5:16 PM";
	}
}
