package org.dsc.component2;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/component1")
public class Component2Controller {


	@GetMapping("/")
    public String getData(){
		
       return "Component2Controller updated on 24th May 2019 1:52 PM";
    }
}
