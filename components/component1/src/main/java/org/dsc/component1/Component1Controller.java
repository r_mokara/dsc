package org.dsc.component1;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/component1")
public class Component1Controller {


	@GetMapping("/")
    public String getData(){
        
       return "Component1Controller updated on 24th May 2019 12:25 PM";
    }
}
